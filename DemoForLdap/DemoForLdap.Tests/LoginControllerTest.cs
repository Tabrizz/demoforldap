﻿using Demo.Controllers;
using Demo.Models;
using DemoForLdap.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace DemoForLdap.Tests
{
    public class LoginControllerTest
    {
        private HttpClient _client;

        public LoginControllerTest()
        {
            var projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent;
            var configPath = Path.Combine(projectDir.FullName, "DemoForLdap\\appsettings.json");

            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<DemoForLdap.Startup>()
                .ConfigureAppConfiguration(conf =>
                {
                    conf.AddJsonFile(configPath);
                }));

            _client = server.CreateClient();
            _client.BaseAddress = new Uri("https://localhost:44397/");
        }

        //[Fact]
        //public async Task User_Login_Is_ValidAsync()
        //{
        //    // Arrange
        //    var request = new
        //    {
        //        Url = "api/account/login",
        //        Body = new
        //        {
        //            UserName = "tabrizg",
        //            Password = "Qwerty12345"
        //        }
        //    };

        //    // Act
        //    var response = await _client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
        //    response.EnsureSuccessStatusCode();
        //    var result = response.Content.ReadAsStringAsync().Result;
        //    var model = JsonConvert.DeserializeObject<OperationResult<TokenViewModel>>(result);

        //    //Assert
        //    Assert.True(model.IsSuccess);
        //    Assert.NotNull(model.Model);
        //    Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.Model.EmployeeFullName);
        //}

        //[Fact]
        //public async Task User_Login_Is_Not_ValidAsync()
        //{
        //    // Arrange
        //    string errorMessage = "Invalid username and/or password";
        //    var request = new
        //    {
        //        Url = "api/account/login",
        //        Body = new
        //        {
        //            UserName = "tabrizg",
        //            Password = "weeeeqwe"
        //        }
        //    };

        //    // Act
        //    var response = await _client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
        //    var result = response.Content.ReadAsStringAsync().Result;
        //    var model = JsonConvert.DeserializeObject<OperationResult<TokenViewModel>>(result);

        //    //Assert
        //    Assert.False(model.IsSuccess);
        //    Assert.Equal(errorMessage, model.ResultText);
        //    Assert.Null(model.Model);
        //}

        //[Fact]
        //public async Task User_Refresh_Token_Successfully()
        //{
        //    // Arrange
        //    var request = new
        //    {
        //        Url = "api/account/refreshToken",
        //        Body = new
        //        {
        //            RefreshToken = "dGFicml6ZyQ4LzIvMjAxOSAzOjAxOjAwIFBNJDE="
        //        }
        //    };

        //    // Act
        //    var response = await _client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
        //    var result = response.Content.ReadAsStringAsync().Result;
        //    var model = JsonConvert.DeserializeObject<OperationResult<TokenViewModel>>(result);

        //    //Assert
        //    Assert.True(model.IsSuccess);
        //    Assert.NotNull(model.Model);
        //    Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.Model.EmployeeFullName);
        //}

        //[Fact]
        //public async Task User_Refresh_Token_Not_Successfully()
        //{
        //    // Arrange
        //    string errorMessage = "The input is not a valid Base-64 string as it contains a non-base 64 character, more than two padding characters, or an illegal character among the padding characters.";
        //    var request = new
        //    {
        //        Url = "api/account/refreshToken",
        //        Body = new
        //        {
        //            RefreshToken = "dGFicml6ZyQ4LzIvMjAxOSAzOjAxOjAwIFBJDE="
        //        }
        //    };

        //    // Act
        //    var response = await _client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
        //    var result = response.Content.ReadAsStringAsync().Result;
        //    var model = JsonConvert.DeserializeObject<OperationResult<TokenViewModel>>(result);

        //    //Assert
        //    Assert.False(model.IsSuccess);
        //    Assert.Null(model.Model);
        //    Assert.Equal(errorMessage, model.ResultText);
        //}

        //[Fact]
        //public async Task User_Get_Profile_Info_Successfully()
        //{
        //    // Arrange
        //    var request = new
        //    {
        //        Url = "api/account/profileInfo"
        //    };
        //    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0YWJyaXpnIiwianRpIjoiZWZhOTNiNTktZTdmMC00NmNjLWI2ZmYtNmYwZWZjNjQxYzI1IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6InRhYnJpemciLCJ1c2VybmFtZSI6InRhYnJpemciLCJlbXBsb3llZV9pZCI6IjU0MyIsInN0YWZmX2lkIjoiMTM3MyIsInBvc2l0aW9uX2lkIjoiMjQyIiwiZGVwYXJ0bWVudF9pZCI6IjIzMCIsInBvc2l0aW9uX25hbWUiOiJLacOnaWsgUHJvcXJhbcOnxLEiLCJkZXBhcnRtZW50X25hbWUiOiJQcm9xcmFtIFTJmW1pbmF0xLEgRGVwYXJ0YW1lbnRpIiwibmFtZSI6IlTJmWJyaXoiLCJzdXJuYW1lIjoiR8O8bG3JmW1tyZlkb3YiLCJmYXRoZXJfbmFtZSI6IljJmWxpbCIsImVtcGxveWVlX2Z1bGxfbmFtZSI6IlTJmWJyaXogR8O8bG3JmW1tyZlkb3YgWMmZbGlsIG_En2x1IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiIiwiaXNzIjoicG9ydGFsLmF0bC5heiIsImF1ZCI6InBvcnRhbC5hdGwuYXoifQ.mjtj1ttGDDBsI8ApC69SibK_VRcWRYnzf15Bf9DN_kQ");

        //    // Act
        //    var response = await _client.PostAsync(request.Url, null);
        //    var result = response.Content.ReadAsStringAsync().Result;
        //    var model = JsonConvert.DeserializeObject<EmployeeProfileModel>(result);

        //    //Assert
        //    Assert.NotNull(model);
        //    Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.EmployeeFullName);
        //}

        //[Fact]
        //public async Task User_Get_Profile_Info_Not_Successfully()
        //{
        //    // Arrange
        //    var request = new
        //    {
        //        Url = "api/account/profileInfo"
        //    };
        //    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0YWJyaXpnIiwianRpIjoiZWZhOTNiNTktZTdmMC00NmNjLWI2ZmYtNmYwZWZjNjQxYzI1IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6InRhYnJpemciLCJ1c2VybmFtZSI6InRhYnJpemciLCJlbXBsb3llZV9pZCI6IjU0MyIsInN0YWZmX2lkIjoiMTM3MyIsInBvc2l0aW9uX2lkIjoiMjQyIiwiZGVwYXJ0bWVudF9pZCI6IjIzMCIsInBvc2l0aW9uX25hbWUiOiJLacOnaWsgUHJvcXJhbcOnxLEiLCJkZXBhcnRtZW50X25hbWUiOiJQcm9xcmFtIFTJmW1pbmF0xLEgRGVwYXJ0YW1lbnRpIiwibmFtZSI6IlTJmWJyaXoiLCJzdXJuYW1lIjoiR8O8bG3JmW1tyZlkb3YiLCJmJfbmFtZSI6IljJmWxpbCIsImVtcGxveWVlX2Z1bGxfbmFtZSI6IlTJmWJyaXogR8O8bG3JmW1tyZlkb3YgWMmZbGlsIG_En2x1IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiIiwiaXNzIjoicG9ydGFsLmF0bC5heiIsImF1ZCI6InBvcnRhbC5hdGwuYXoifQ.mjtj1ttGDDBsI8ApC69SibK_VRcWRYnzf15Bf9DN_kQ");

        //    // Act
        //    var response = await _client.PostAsync(request.Url, null);

        //    //Assert
        //    Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        //}

        [Theory(DisplayName = "User_Login_Successfully")]
        [InlineData("tabrizg", "Qwerty12345", "Tabriz.Gulmammadov@atltech.az")]
        public void UserLoginSuccess(string userName, string password, string email)
        {
            //Arrange
            var mock = new Mock<ILdapService>();
            mock.Setup(p => p.Authenticate($"atl\\{userName}", password)).Returns(true);
            mock.Setup(p => p.GetUserByUserName(userName)).Returns(new LdapUser()
            {
                Email = email
            });

            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtKey")]).Returns("ATLPORTAL_SUPER_SECRET_KEY");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtExpireMins")]).Returns("1440");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtIssuer")]).Returns("portal.atl.az");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtAudience")]).Returns("portal.atl.az");

            //Act
            AccountController account = new AccountController(mock.Object, mockConfSection.Object);
            var result = account.Login(new SigninViewModel()
            {
                UserName = userName,
                Password = password
            }).GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<OperationResult<TokenViewModel>>(
        actionResult.Value);
            Assert.True(model.IsSuccess);
            Assert.NotNull(model.Model);
            Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.Model.EmployeeFullName);
        }

        [Theory(DisplayName = "User_Login_Not_Successfully")]
        [InlineData("tabrizg", "Qwerty122345", "Invalid username and/or password")]
        public void UserLoginNotSuccess(string userName, string password, string errorMessage)
        {
            //Arrange
            var mock = new Mock<ILdapService>();
            mock.Setup(p => p.Authenticate($"atl\\{userName}", password)).Returns(false);

            //Act
            AccountController account = new AccountController(mock.Object, null);
            var result = account.Login(new SigninViewModel()
            {
                UserName = userName,
                Password = password
            }).GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<BadRequestObjectResult>(result);
            var model = Assert.IsAssignableFrom<OperationResult>(
        actionResult.Value);
            Assert.False(model.IsSuccess);
            Assert.Equal(errorMessage, model.ResultText);
        }

        [Theory(DisplayName = "User_Refresh_Token_Successfully")]
        [InlineData("dGFicml6ZyQ4LzIvMjAxOSAzOjAxOjAwIFBNJDE=")]
        public void UserRefreshTokenSuccessfully(string refreshToken)
        {
            //Arrange
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtKey")]).Returns("ATLPORTAL_SUPER_SECRET_KEY");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtExpireMins")]).Returns("1440");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtIssuer")]).Returns("portal.atl.az");
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "Authentication:JwtAudience")]).Returns("portal.atl.az");

            //Act
            AccountController account = new AccountController(null, mockConfSection.Object);
            var result = account.Refresh(new RefreshTokenViewModel()
            {
                RefreshToken = refreshToken
            }).GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<OperationResult<TokenViewModel>>(
        actionResult.Value);
            Assert.True(model.IsSuccess);
            Assert.NotNull(model.Model);
            Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.Model.EmployeeFullName);
        }

        [Theory(DisplayName = "User_Refresh_Token_Not_Successfully")]
        [InlineData("dGFicml6ZyQ4LzIvMjAxOSAzOjAxOjAwewIFBNJDE=")]
        public void UserRefreshTokenNotSuccessfully(string refreshToken)
        {
            //Act
            AccountController account = new AccountController(null, null);
            var result = account.Refresh(new RefreshTokenViewModel()
            {
                RefreshToken = refreshToken
            }).GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<BadRequestObjectResult>(result);
            var model = Assert.IsAssignableFrom<OperationResult>(
        actionResult.Value);
            Assert.False(model.IsSuccess);
            Assert.NotNull(model.ResultText);
        }

        [Theory(DisplayName = "Get_User_Profile_Info_Successfully")]
        [InlineData("tabrizg")]
        public void GetUserProfileInfoSuccessfully(string userName)
        {
            //Arrange
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, userName),
            }));

            //Act
            AccountController account = new AccountController(null, null);
            account.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            var result = account.ProfileInfo().GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<EmployeeProfileModel>(
        actionResult.Value);
            Assert.NotNull(model);
            Assert.Equal("Təbriz Gülməmmədov Xəlil oğlu", model.EmployeeFullName);
        }

        [Theory(DisplayName = "Get_User_Profile_Info_Not_Successfully")]
        [InlineData("tabriz")]
        public void GetUserProfileInfoNotSuccessfully(string userName)
        {
            //Arrange
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, userName),
            }));

            //Act
            AccountController account = new AccountController(null, null);
            account.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            var result = account.ProfileInfo().GetAwaiter().GetResult();

            //Assert
            var actionResult = Assert.IsType<BadRequestObjectResult>(result);
            var model = Assert.IsAssignableFrom<OperationResult>(
        actionResult.Value);
            Assert.NotNull(model);
            Assert.False(model.IsSuccess);
        }
    }
}
