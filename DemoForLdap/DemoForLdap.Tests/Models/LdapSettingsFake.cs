﻿using Demo.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoForLdap.Tests.Models
{
    public class LdapSettingsFake
    {
        public string ServerName => "dc01.atl.lan";

        public int ServerPort => 636;

        public bool UseSSL => true;

        public string SearchBase => "DC=atl,DC=lan";

        public string ContainerName => "DC=atl,DC=lan";

        public string DomainName => "atl.lan";

        public string DomainDistinguishedName => "DC=atl,DC=lan";

        public LdapCredentials Credentials = new LdapCredentials() {  DomainUserName ="atl\\portal", Password = "J=3F9q#z"};
    }
}
