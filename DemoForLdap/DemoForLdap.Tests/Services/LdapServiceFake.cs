﻿using Demo.Models;
using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.Text;
using LdapEntry = Demo.Models.LdapEntry;
using LdapSettingsFake = DemoForLdap.Tests.Models.LdapSettingsFake;

namespace DemoForLdap.Tests.Services
{
    public class LdapServiceFake : ILdapService
    {
        private readonly LdapSettingsFake _ldapSettings;
        public LdapServiceFake(LdapSettingsFake ldapSettings)
        {
            _ldapSettings = ldapSettings;
        }

        public void AddUser(LdapUser user, string password)
        {
            throw new NotImplementedException();
        }

        public bool Authenticate(string distinguishedName, string password)
        {
            using (var ldapConnection = new LdapConnection() { SecureSocketLayer = true })
            {
                ldapConnection.Connect(this._ldapSettings.ServerName, this._ldapSettings.ServerPort);

                try
                {
                    ldapConnection.Bind(distinguishedName, password);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public void DeleteUser(string distinguishedName)
        {
            throw new NotImplementedException();
        }

        public LdapUser GetAdministrator()
        {
            throw new NotImplementedException();
        }

        public ICollection<LdapUser> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public ICollection<LdapEntry> GetGroups(string groupName, bool getChildGroups = false)
        {
            throw new NotImplementedException();
        }

        public LdapUser GetUserByUserName(string userName)
        {
            throw new NotImplementedException();
        }

        public ICollection<LdapUser> GetUsersByEmailAddress(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public ICollection<LdapUser> GetUsersInGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public ICollection<LdapUser> GetUsersInGroups(ICollection<Demo.Models.LdapEntry> groups = null)
        {
            throw new NotImplementedException();
        }

        ICollection<Demo.Models.LdapEntry> ILdapService.GetGroups(string groupName, bool getChildGroups)
        {
            throw new NotImplementedException();
        }
    }
}
