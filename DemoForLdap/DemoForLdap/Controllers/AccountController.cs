﻿using Demo.Models;
using DemoForLdap;
using DemoForLdap.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private ILdapService _ldapService;
        private readonly IConfiguration _configuration;
        //private ApplicationDataContext _context;

        public AccountController(ILdapService ldapService, IConfiguration configuration)
        {
            _ldapService = ldapService;
            _configuration = configuration;
            //_context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("salam");
        }

        [HttpPost("addUser")]
        public async Task<IActionResult> AddUser([FromBody] User user)
        {
            try
            {
                if (user.Id > 0)
                {
                    //_context.Entry(user).State = EntityState.Modified;
                    //await _context.SaveChangesAsync();

                    IActionResult users = await GetAllUsers();
                    return Ok(new
                    {
                        IsSuccess = true,
                        Model = users
                    });
                }
                else
                {
                    bool result = await AddUserToDB(user);

                    if (result)
                    {
                        IActionResult users = await GetAllUsers();
                        return Ok(new
                        {
                            IsSuccess = true,
                            Model = users
                        });
                    }
                    else
                    {
                        return Ok(new OperationResult()
                        {
                            IsWarning = true,
                            IsSuccess = false,
                            ResultText = "Given user exist in database."
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error saving user data. Message:{1} Trace:{2}", ex.Message, ex.StackTrace);
                return Ok(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = ex.Message
                });
            }
        }

        [HttpDelete("deleteUser")]
        public async Task<IActionResult> DeleteUser([FromBody] User user)
        {
            try
            {
                //_context.Users.Remove(user);
                //await _context.SaveChangesAsync();
                IActionResult users = await GetAllUsers();
                return Ok(new
                {
                    IsSuccess = true,
                    Model = users
                });
            }
            catch (Exception ex)
            {
                Log.Error("Error removing user data. Message:{1} Trace:{2}", ex.Message, ex.StackTrace);
                return Ok(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = ex.Message
                });
            }
        }

        /// <summary>
        /// Creates a TodoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Account/login
        ///     {
        ///        "userName": "admin",
        ///        "password": "admin"
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response> 
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] SigninViewModel model)
        {
            string errorMessage = "Invalid username and/or password";

            try
            {
                bool authenticated = _ldapService.Authenticate(string.Format("atl\\{0}", model.UserName), model.Password);

                if (authenticated)
                {
                    LdapUser authenticatedUser = _ldapService.GetUserByUserName(model.UserName);
                    authenticatedUser.SamAccountName = model.UserName;
                    TokenViewModel token = GenerateToken(authenticatedUser).GetAwaiter().GetResult();

                    //add user to database
                    User user = new User
                    {
                        EmployeeFullName = token.EmployeeFullName,
                        DepartmentName = token.DepartmentName,
                        PositionName = token.PositionName
                    };
                    bool result = await AddUserToDB(user);

                    return Ok(new OperationResult<TokenViewModel>()
                    {
                        IsSuccess = true,
                        Model = token
                    });
                }
                else
                {
                    return BadRequest(new OperationResult()
                    {
                        IsWarning = true,
                        IsSuccess = false,
                        ResultText = errorMessage
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error saving user data. Message:{1} Trace:{2}", ex.Message, ex.StackTrace);
                return BadRequest(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = ex.Message
                });
            }
        }

        public async Task<bool> AddUserToDB(User model)
        {
            try
            {

                bool result = false;

                //User user = _context.Users.ToList().FirstOrDefault(c => c.EmployeeFullName == model.EmployeeFullName);
                //if (user == null)
                //{
                //    await _context.Users.AddAsync(model);
                //    result = await _context.SaveChangesAsync() > 0;
                //}

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Error saving user data. Message:{1} Trace:{2}", ex.Message, ex.StackTrace);
                return false;
            }

        }

        [HttpGet("getAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                //List<User> users = await _context.Users.ToListAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error("Error saving user data. Message:{1} Trace:{2}", ex.Message, ex.StackTrace);
                return Ok(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = ex.Message
                });
            }

        }

        [HttpPost]
        private async Task<TokenViewModel> GenerateToken(LdapUser user)
        {
            ApiClient apiClient = new ApiClient();
            EmployeeShortInfo employeeInfo = await apiClient.GetEmployeeShortInfoByUserName(user.SamAccountName);

            string userRoles = "";
            if ((new List<string>() { "farahnazm", "ceyhunah", "anarm", "zumrudh", "saidam" }).Contains(user.SamAccountName.ToLower()))
            {
                userRoles = "admin";
            }

            List<Claim> claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.SamAccountName),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Name, user.SamAccountName),
                new Claim("username", user.SamAccountName),
                new Claim("employee_id", employeeInfo.EmployeeId.ToString()),
                new Claim("staff_id", employeeInfo.StaffId.ToString()),
                new Claim("position_id",employeeInfo.PositionId.ToString()),
                new Claim("department_id", employeeInfo.DepartmentId.ToString()),
                new Claim("position_name", employeeInfo.PositionName),
                new Claim("department_name", employeeInfo.DepartmentName),
                new Claim("name", employeeInfo.EmployeeName),
                new Claim("surname", employeeInfo.EmployeeSurname),
                new Claim("father_name", employeeInfo.EmployeeFatherName),
                new Claim("employee_full_name", employeeInfo.EmployeeFullName),
                new Claim(ClaimTypes.Role,userRoles)
            };

            var keydsa = _configuration["Authentication:JwtKey"];
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:JwtKey"]));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime expires = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["Authentication:JwtExpireMins"]));
            JwtSecurityToken token = new JwtSecurityToken(_configuration["Authentication:JwtIssuer"], _configuration["Authentication:JwtAudience"],
                                            claims,
                                            //expires: expires,
                                            signingCredentials: creds
                                            );

            string refreshToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            //try
            //{

            //    var portalUser = _db.Users.GetByUserName(user.SamAccountName, new UserView());
            //    if (portalUser == null)
            //    {
            //        portalUser = new BLL.DbModel.User
            //        {
            //            UserName = user.SamAccountName,
            //            //DisplayName = user.DisplayName,
            //            //EmailAddress = user.EmailAddress,
            //            RefreshToken = refreshToken
            //        };

            //        _db.Users.Insert(portalUser, new UserView());


            //    }
            //    else
            //    {
            //        portalUser.RefreshToken = refreshToken;
            //        _db.Users.Update(portalUser, new UserView());
            //    }

            //    _db.Save();

            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}


            return new TokenViewModel
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                DisplayName = employeeInfo.EmployeeName + " " + employeeInfo.EmployeeSurname,
                EmployeeId = employeeInfo.EmployeeId,
                StaffId = employeeInfo.StaffId,
                EmailAddress = user.EmailAddress,
                RefreshToken = refreshToken,
                Roles = new List<string> { userRoles },

                DepartmentName = employeeInfo.DepartmentName,
                EmployeeFullName = employeeInfo.EmployeeFullName,
                PositionName = employeeInfo.PositionName
            };
        }

        [HttpPost("refreshToken")]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var rToken = Convert.FromBase64String(model.RefreshToken);
                var userName = UTF8Encoding.UTF8.GetString(rToken).Split('$')[0];

                var token = await GenerateToken(new LdapUser
                {
                    SamAccountName = userName
                });
                return Ok(new OperationResult<TokenViewModel>()
                {
                    IsSuccess = true,
                    Model = token
                });
            }
            catch (Exception exception)
            {
                return BadRequest(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = exception.Message
                });
            }
        }

        [Authorize]
        [HttpPost("profileInfo")]
        public async Task<IActionResult> ProfileInfo()
        {
            try
            {
                var userName = HttpContext.User.Identity.Name;

                ApiClient apiClient = new ApiClient();
                EmployeeShortInfo employeeInfo = await apiClient.GetEmployeeShortInfoByUserName(userName);
                var userInfo = new EmployeeProfileModel
                {
                    EmployeeFullName = employeeInfo.EmployeeFullName,
                    DepartmentName = employeeInfo.DepartmentName,
                    PositionName = employeeInfo.PositionName,
                    Email = employeeInfo.Email,
                    BirthDate = employeeInfo.Birthday
                };

                Log.Information($"Employee info get successfully. User: {userInfo.EmployeeFullName}");
                return Ok(userInfo);
            }
            catch (Exception exception)
            {
                Log.Error($"Error created when employee info get. Error:{exception.ToString()}");
                return BadRequest(new OperationResult()
                {
                    IsSuccess = false,
                    ResultText = exception.Message
                }); ;
            }
        }

        public class Flow
        {
            public ICollection<NodeData> NodeDataArray { get; set; }
            public ICollection<LinkData> LinkDataArray { get; set; }
        }

        public class NodeData
        {
            public int Key { get; set; }
            public string Text { get; set; }
            public string Color { get; set; }
        }

        public class LinkData
        {
            public int From { get; set; }
            public int To { get; set; }
        }

        [HttpPost("getFlow")]
        public IActionResult GetFlowStructure([FromBody] Flow flow)
        {
            return Ok(flow);
        }
    }
}