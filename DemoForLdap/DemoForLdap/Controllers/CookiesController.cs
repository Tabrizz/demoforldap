﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace DemoForLdap.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CookiesController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CookiesController(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("readData")]
        public IActionResult Index()
        {
            //set the key value in Cookie  
            Set("kay", "Hello from cookie", 10);
            //read cookie from IHttpContextAccessor  
            string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["kay"];
            //read cookie from Request object  
            string cookieValueFromReq = Request.Cookies["kay"];
            return Ok();
        }

        public class ReadClass
        {
            public string key { get; set; }
        }

        /// <summary>  
        /// Get the cookie  
        /// </summary>  
        /// <param name="model">Key </param>  
        /// <returns>string value</returns>  
        [HttpPost("getKey")]
        public IActionResult Get([FromBody]ReadClass model)
        {
            var data = Request.Cookies[model.key];
            return Ok(data.ToString());
        }
        /// <summary>  
        /// set the cookie  
        /// </summary>  
        /// <param name="key">key (unique indentifier)</param>  
        /// <param name="value">value to store in cookie object</param>  
        /// <param name="expireTime">expiration time</param>  
        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }
        /// <summary>  
        /// Delete the key  
        /// </summary>  
        /// <param name="key">Key</param>  
        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }



        [HttpPost("writeCookie")]
        public IActionResult WriteCookie([FromBody] CookieModel model)
        {
            if (model.IsPersistent)
            {
                CookieOptions options = new CookieOptions();
                options.Expires = DateTime.Now.AddDays(1);
                HttpContext.Response.Cookies.Append(model.CookieName, model.CookieValue, options);
            }
            else
            {
                HttpContext.Response.Cookies.Append(model.CookieName, model.CookieValue);
            }
            return Ok();
        }

        [HttpGet("readCookie")]
        public IActionResult ReadCookie()
        {
            string val = HttpContext.Request.Cookies["kay"];

            return Ok(val);
        }

        [HttpPost("uploadFiles")]
        public async Task<IActionResult> UploadAsync(IFormCollection files)
        {
            var webPath = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "uploads"));
            foreach (var file in files.Files)
            {
                var path = Path.Combine(webPath?.Root + file.FileName);

                // This stream the physical file to the allocate wwwroot/ImageFiles folder
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            
            return Ok("Hey");
        }
    }

    public class CookieModel
    {
        public string CookieName { get; set; }
        public string CookieValue { get; set; }
        public bool IsPersistent { get; set; }
    }
}