﻿using DemoForLdap.Models.Questions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Hubs
{
    public interface IQuestionHub
    {
        Task QuestionAdded(Question question);
        Task QuestionScoreChange(Guid questionId, int score);
        Task AnswerCountChange(Guid questionId, int answerCount);
        Task AnswerAdded(Answer answer);
    }
}
