﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace DemoForLdap.Hubs
{
    public class QuestionHub: Hub<IQuestionHub>
    {
        private readonly ILogger logger;
        public QuestionHub(ILogger<QuestionHub> logger)
        {
            this.logger = logger;
        }
        public async Task JoinQuestionGroup(Guid questionId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, questionId.ToString());
        }
        public async Task LeaveQuestionGroup(Guid questionId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, questionId.ToString());
        }
    }
}
