﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public class EmployeeProfileModel
    {
        public string EmployeeFullName { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
