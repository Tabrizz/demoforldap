﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public partial class ApiClient
    {
        public async Task<List<EmployeeShortInfo>> GetEmployeeShortInfo(string employeeId, string staffId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/EmployeeService/GetEmployeeListShotInfo"), employeeId + "&" + staffId);
            return await GetAsync<List<EmployeeShortInfo>>(requestUrl);
        }

        //public async Task<List<EmployeeShortInfo>> GetEmployeeMoreInfo(string personId, string userId)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "/EmployeeService/GetEmployeeListShotInfo"), "personId="+ personId + "&userId=" + userId);
        //    return await GetAsync<List<EmployeeShortInfo>>(requestUrl);
        //}

        public async Task<EmployeeShortInfo> GetEmployeeShortInfoByUserName(string userName)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture, "/EmployeeService/GetEmployeeShortInfoByUserName"), "userName=" + userName);

            return await GetAsync<EmployeeShortInfo>(requestUrl);
        }

        //public async Task<DataTableSourceList<EmployeeInfo>> GetEmployeeList(EmployeeFilter employeeFilter)
        //{
        //    var employeeJson = JsonConvert.SerializeObject(employeeFilter);
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "/EmployeeService/GetEmployeeList"), "employeeJson=" + employeeJson);
        //    return await GetAsync<DataTableSourceList<EmployeeInfo>>(requestUrl);
        //}


        //public async Task<DataTableSourceList<EmployeeInfo>> GetEmployeeKeyValue(EmployeeFilter employeeFilter)
        //{
        //    var employeeJson = JsonConvert.SerializeObject(employeeFilter);
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "/EmployeeService/GetEmployeeKeyValue"), "employeeJson=" + employeeJson);
        //    return await GetAsync<DataTableSourceList<EmployeeInfo>>(requestUrl);
        //}

        //public async Task<List<PositionInfo>> GetPositions(string name, int skip, int take)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetPositions"), "name=" + name + "&skip=" + skip + "&take=" + take);
        //    return await GetAsync<List<PositionInfo>>(requestUrl);
        //}

        //public async Task<List<TeamVacation>> GetTeamVacationByStaff(int staffId)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetTeamVacationByStaff"), "staffId=" + staffId);
        //    return await GetAsync<List<TeamVacation>>(requestUrl);
        //}

        //public async Task<List<TeamEmployee>> GetTeamEmployeeByStaff(int staffId)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetTeamEmployeeByStaff"), "staffId=" + staffId);
        //    return await GetAsync<List<TeamEmployee>>(requestUrl);
        //}


        //public async Task<List<DepartmentInfo>> GetDepartments(string name, int skip, int take)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetDepartments"), "name=" + name + "&skip=" + skip + "&take=" + take);
        //    return await GetAsync<List<DepartmentInfo>>(requestUrl);
        //}

        //public async Task<List<int>> GetStaffIds(EmployeeFilterModel employeeFilterModel)
        //{
        //    var employeeFilterModelJson = JsonConvert.SerializeObject(employeeFilterModel);
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetStaffIds"), "employeeFilterModelJson=" + employeeFilterModelJson);
        //    return await GetAsync<List<int>>(requestUrl);
        //}

        //public async Task<VacationDetail> GetVacationDetail(VacationDetailFilter vacationDetailFilter)
        //{
        //    var vacationDetailFilterJson = JsonConvert.SerializeObject(vacationDetailFilter, new IsoDateTimeConverter { DateTimeFormat = "dd.MM.yyyy" });
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //       "/EmployeeService/GetVacationDetail"), "vacationDetailFilterJson=" + vacationDetailFilterJson);
        //    var a = await GetAsync<VacationDetail>(requestUrl);
        //    return a;
        //}

        //public async Task<EarnedVacationDayCount> GetEarnedVacationDayCount(int employeeId)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //      "/EmployeeService/GetEarnedVacationDayCount"), "employeeId=" + employeeId);
        //    return await GetAsync<EarnedVacationDayCount>(requestUrl);
        //}

        //public async Task<OperationResult> CheckVacationPosibility(VacationDto model)
        //{
        //    var modelJson = JsonConvert.SerializeObject(model, new IsoDateTimeConverter { DateTimeFormat = "dd.MM.yyyy" });

        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //      "/EmployeeService/CheckVacationPosibility"), "vacationJson=" + modelJson);
        //    return await GetAsync<OperationResult>(requestUrl);
        //}

        //public async Task<OperationResult> CheckBusinessTripPosibility(BusinessTripRequestDto model)
        //{
        //    var modelJson = JsonConvert.SerializeObject(model, new IsoDateTimeConverter { DateTimeFormat = "dd.MM.yyyy" });

        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //      "/EmployeeService/CheckBusinessTripPosibility"), "businessTripJson=" + modelJson);
        //    return await GetAsync<OperationResult>(requestUrl);
        //}


        //public async Task<OperationResult> SaveVacation(VacationDto model)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "EmployeeService/SaveVacation"));
        //    return await PostAsync<OperationResult, VacationDto>(requestUrl, model);
        //}

        //public async Task<OperationResult> SaveBusinessTrip(BusinessTripRequestDto model)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "EmployeeService/SaveBusinessTrip"));
        //    return await PostAsync<OperationResult, BusinessTripRequestDto>(requestUrl, model);
        //}

        //public async Task<OperationResult> SaveVacation(DayOffRequestDto model)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "EmployeeService/SaveDayOff"));
        //    return await PostAsync<OperationResult, DayOffRequestDto>(requestUrl, model);
        //}

        //public async Task<Message<UsersModel>> SaveUser(UsersModel model)
        //{
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "User/SaveUser"));
        //    return await PostAsync<UsersModel>(requestUrl, model);
        //}
    }


    public partial class ApiClient
    {

        private readonly HttpClient _httpClient;
        private Uri BaseEndpoint { get; set; } = new Uri("http://hrm.atl.lan");
        // private Uri BaseEndpoint { get; set; } = new Uri("http://localhost:56247");

        // public ApiClient(Uri baseEndpoint)
        public ApiClient()
        {
            //if (baseEndpoint == null)
            // {
            //    throw new ArgumentNullException("baseEndpoint");
            //}
            //  BaseEndpoint = baseEndpoint;
            _httpClient = new HttpClient();
        }

        /// <summary>  
        /// Common method for making GET calls  
        /// </summary>  
        private async Task<T> GetAsync<T>(Uri requestUrl)
        {
            addHeaders();
            var response = await _httpClient.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead);
            response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data, new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            });
        }

        /// <summary>  
        /// Common method for making POST calls  
        /// </summary>  
        private async Task<T> PostAsync<T>(Uri requestUrl, T content)
        {
            addHeaders();
            var response = await _httpClient.PostAsync(requestUrl.ToString(), CreateHttpContent<T>(content));
            response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data);
        }
        private async Task<T1> PostAsync<T1, T2>(Uri requestUrl, T2 content)
        {
            addHeaders();
            var response = await _httpClient.PostAsync(requestUrl.ToString(), CreateHttpContent<T2>(content));
            response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T1>(data);
        }

        private Uri CreateRequestUri(string relativePath, string queryString = "")
        {
            relativePath = "/PLUGINS/Hrm/" + relativePath;
            var endpoint = new Uri(BaseEndpoint, relativePath);
            var uriBuilder = new UriBuilder(endpoint);
            uriBuilder.Query = queryString;
            return uriBuilder.Uri;
        }

        private HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content, new IsoDateTimeConverter { DateTimeFormat = "dd.MM.yyyy" });
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private static JsonSerializerSettings MicrosoftDateFormatSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat

                };
            }
        }

        private void addHeaders()
        {
            _httpClient.DefaultRequestHeaders.Remove("userIP");
            _httpClient.DefaultRequestHeaders.Add("userIP", "192.168.1.1");
        }
    }
}
