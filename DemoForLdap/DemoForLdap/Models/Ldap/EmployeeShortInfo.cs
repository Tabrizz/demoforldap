﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public class EmployeeShortInfo
    {
        public int EmployeeId { get; set; }
        public string Email { get; set; }
        public int StaffId { get; set; }
        public int PositionId { get; set; }
        public int DepartmentId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public string EmployeeFatherName { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
        public DateTime WorkStatDate { get; set; }
        public DateTime LastJobStartDate { get; set; }
        public DateTime Birthday { get; set; }
        public string EmployeeFullName { get; set; }
    }
}
