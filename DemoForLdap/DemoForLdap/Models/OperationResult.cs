﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public class OperationResult<T> : OperationResultDetail where T : class, new()
    {
        public T Model { get; set; }
        public OperationResult(bool isSuccess = true, bool isWarning = false, string resultText = null, T model = null)
            : base(isSuccess, isWarning, resultText)
        {
            Model = model;
        }
    }

    public class OperationResult : OperationResultDetail
    {
        public OperationResult(bool isSuccess = true, bool isWarning = false, string resultText = null)
            : base(isSuccess, isWarning, resultText)
        {
        }
    }

    public class OperationResultDetail
    {
        public string ResultText { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsWarning { get; set; }
        public OperationResultDetail(bool isSuccess = true, bool isWarning = false, string resultText = null)
        {
            ResultText = resultText;
            IsSuccess = isSuccess;
            IsWarning = isWarning;
        }
    }
}
