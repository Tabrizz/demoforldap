﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public class RefreshTokenViewModel
    {
        public string RefreshToken { get; set; }
    }
}
