﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Models
{
    public class SigninViewModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }

    public class SigninValidator: AbstractValidator<SigninViewModel>
    {
        public SigninValidator()
        {
            RuleFor(x => x.Password).NotEmpty().NotNull().MinimumLength(6);
            RuleFor(x => x.UserName).NotEmpty().NotNull().MinimumLength(3);
        }
    }
}
