﻿using System.Collections.Generic;

namespace Demo.Models
{
    public class TokenViewModel
    {
        public string AccessToken { get; set; }
        public string DisplayName { get; set; }
        public int EmployeeId { get; set; }
        public int StaffId { get; set; }
        public string EmailAddress { get; set; }
        public string RefreshToken { get; set; }
        public List<string> Roles { get; set; }

        public string EmployeeFullName { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
    }
}