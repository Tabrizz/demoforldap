﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoForLdap.Models
{
    public class User
    {
        public int Id { get; set; }
        public string EmployeeFullName { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
    }

    public class UserValidator: AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.DepartmentName).NotNull().NotEmpty().MinimumLength(2);
            RuleFor(x => x.PositionName).NotNull().NotEmpty().MinimumLength(3);
            RuleFor(x => x.EmployeeFullName).NotNull().NotEmpty().MinimumLength(4);
        }
    }
}
