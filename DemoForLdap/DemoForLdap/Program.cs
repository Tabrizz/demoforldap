﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace DemoForLdap
{
#pragma warning disable CS1591
    public class Program
    {
        private static string _environmentName;

        public static IConfiguration configuration { get; } = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true, reloadOnChange: true)
                        .Build();
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                                  .ReadFrom
                                  .Configuration(configuration)
                                  .Enrich.FromLogContext()
                                  .CreateLogger();

            try
            {
                CreateWebHostBuilder(args).Run();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHost CreateWebHostBuilder(string[] args) =>
             WebHost.CreateDefaultBuilder(args)
             .UseStartup<Startup>()
             .UseConfiguration(configuration)
             .UseSerilog()
             .Build();
    }
#pragma warning disable
}
