import axios from "axios";

const client = axios.create({
  baseURL: "https://localhost:44397/api/Questions",
  json: true
});

export default {
  async execute(method, resource, data) {
    return await client({
      method,
      url: resource,
      data
    })
      .then(req => {
        return req.data;
      })
      .catch(error => console.log(error));
  },
  getAllQuestions() {
    return this.execute("get", "/getAllQuestions");
  },
  getQuestion(data) {
    return this.execute("get", `/${data}/getQuestion/`);
  },
  addQuestion(data) {
    return this.execute("post", "/addQuestion", data);
  },
  addAnswer(data) {
    console.log(data);
    return this.execute("post", "answer", data);
  },
  upvoteQuestion(data) {
    return this.execute("patch", `/${data}/upvote`);
  },
  downvoteQuestion(data) {
    return this.execute("patch", `/${data}/downvote`);
  },
  addCookie() {
    return axios.post("https://localhost:44397/api/Cookies/writeCookie", {
      CookieName: "kay",
      CookieValue: "kay13131312",
      IsPersistent: true
    });
  },
  uploadFiles(files) {
    return axios.post(
      "https://localhost:44397/api/Cookies/uploadFiles",
      files,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
  }
};
