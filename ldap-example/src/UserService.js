import axios from 'axios'

const client = axios.create({
  baseURL: 'https://localhost:44397/api/Account',
  json: true
})

export default {
  async execute(method, resource, data) {
    const accessToken = await window.localStorage.getItem('token');
    return await client({
      method,
      url: resource,
      data,
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    }).then(req => {
      return req.data
    }).catch(error => console.log(error));
  },
  getAllUsers() {
    return this.execute('get', '/getAllUsers')
  },
  addUser(data) {
    return this.execute('post', '/addUser', data)
  },
  deleteUser(data) {
    return this.execute('delete', '/deleteUser', data)
  },
  login(data) {
    return this.execute('post', '/login', data)
  },
  getFlow(data) {
    return this.execute('post', '/getFlow', data)
  }
}

