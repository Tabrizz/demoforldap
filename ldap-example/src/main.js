import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Vuex from 'vuex'
import VModal from 'vue-js-modal'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App'
import router from './router/index'

Vue.use(VModal)
Vue.use(BootstrapVue)
Vue.use(Vuex)

Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    accessToken: localStorage.getItem('token'),
    refreshToken: ''
  },
  mutations: {
    setTokens(state, rToken) {
      state.refreshToken = rToken;
    }
  },
  actions: {
    setTokensCommit({commit}, rToken){
      commit('setTokens', rToken);
    }
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  store,
  router,
})
