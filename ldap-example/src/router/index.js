import Vue from 'vue'
import Router from 'vue-router'
import Hello from '../components/Hello'
import UserProfile from '../components/UserProfile'
import Sample from '../components/Sample'
import TestCompForAdvanced from '../components/TestCompForAdvanced'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/user-profile',
      name: 'UserProfile',
      component: UserProfile
    },
    {
      path: '/sample',
      name: 'Sample',
      component: Sample
    },
    {
      path: '/test',
      name: 'Test',
      component: TestCompForAdvanced
    },
  ]
})
